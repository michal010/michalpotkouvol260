﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereCreator : MonoBehaviour
{
    [Header("Minimalny odstęp od kamery")]
    public float MinimumDistanceFromCamera = 10f;
    [Header("Maksymalny odstęp od kamery")]
    public float MaximumDistanceFromCamera = 50f;
    public float TimeBetweenSpawns = 0.25f;

    public int SpheresLimit = 0;
    public static SphereCreator Instance;

    public GameObject SpherePrefab;

    private Queue<GameObject> SphereObjectPool;
    public static int EnabledSpheres = 0;

    Camera cam;
    // Start is called before the first frame update

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            cam = Camera.main;
            SphereObjectPool = new Queue<GameObject>();
            for (int i = 0; i < SpheresLimit; i++)
            {
                GameObject go = Instantiate(SpherePrefab);
                go.AddComponent<Attractor>();
                go.SetActive(false);
                SphereObjectPool.Enqueue(go);
            }
        }
        else
        {
            Destroy(this);
        }
    }

    private float elapsedTime = 0;
    // Update is called once per frame
    void Update()
    {
        elapsedTime += Time.deltaTime;
        if(elapsedTime >= TimeBetweenSpawns)
        {
            if(CanSpawnSphere())
            {
                SpawnSphere();
            }
            else
            {
                //reverse forces
                UniversalGravitySettings.SetFoce(UniversalForceType.Push);
                UniversalGravitySettings.SetAttractorsCombineFlag(false);
            }
            elapsedTime = 0;
        }
    }

    private bool CanSpawnSphere()
    {
        return EnabledSpheres < SpheresLimit;
    }

    private void SpawnSphere()
    {
        GameObject obj = DequeueObject();
        obj.transform.position = GetRandomPointInCameraPOV();
        EnabledSpheres++;
        obj.SetActive(true);
    }

    public void EnqueueObject(GameObject obj)
    {
        EnabledSpheres--;
        obj.SetActive(false);
        SphereObjectPool.Enqueue(obj);
    }
    public GameObject DequeueObject()
    {
        GameObject objectToSpawn = SphereObjectPool.Dequeue();
        //reset
        objectToSpawn.transform.Resize(1);
        objectToSpawn.GetComponent<Attractor>().ResetAttributes();
        return objectToSpawn;
    }

    public void SpawnSpheresWithRandomForce(int sphereCount, Vector3 position)
    {
        for (int i = 0; i < sphereCount; i++)
        {
            if (!CanSpawnSphere())
                break;
            GameObject go = DequeueObject();
            Vector3 Direction = UnityEngine.Random.onUnitSphere;
            Direction.z = 0;
            Vector3 force = Direction 
                * UnityEngine.Random.Range(UniversalGravitySettings.Instance.OnMassTreshholdExplosionMinForce, UniversalGravitySettings.Instance.OnMassTreshholdExplosionMaxForce)
                * UniversalGravitySettings.Instance.GravityScale;
            go.transform.position = position + (Direction * 2f);
            go.GetComponent<Collider2D>().isTrigger = true;
            EnabledSpheres++;
            go.SetActive(true);
            go.GetComponent<Attractor>().ExplosionBehaviour(force);

            //Debug.Log("Spawned position" + position);
            //Vector3 Direction = UnityEngine.Random.onUnitSphere;
            //GameObject go = Instantiate(SpherePrefab, position + (Direction * 2f), Quaternion.identity);
            //go.GetComponent<Collider>().isTrigger = true;
            //Vector3 force = Direction * Time.deltaTime * UnityEngine.Random.Range(UniversalGravitySettings.Instance.OnMassTreshholdReachedMinForce, UniversalGravitySettings.Instance.OnMassTreshholdReachedMaxForce); ;
            //go.AddComponent<Attractor>().ExplosionBehaviour(force);
            
        }
    }

    public Vector3 GetRandomPointInCameraPOV()
    {
        Vector3 result = new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(MinimumDistanceFromCamera, MaximumDistanceFromCamera));
        result = cam.ViewportToWorldPoint(result);
        return result;
    }
}
