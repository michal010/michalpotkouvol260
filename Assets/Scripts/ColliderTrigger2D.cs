﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ColliderTrigger2D : MonoBehaviour
{
    public UnityEvent OnCollision;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        OnCollision?.Invoke();
        this.enabled = false;
    }
}
