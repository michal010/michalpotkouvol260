﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SlingshotState { Loaded, Firing, Fired, Drag, Reloading}
public class Slingshot : MonoBehaviour
{
    //refs
    public Transform RubberAnchorOne;
    public Transform RubberAnchorTwo;
    public GameObject projectilePreviewRef;
    public GameObject ProjectilePrefab;
    public Animator CameraAnimController;
    public CinemachineVirtualCamera ProjectileCam;
    //settings
    public float PowerScaling;
    public float DragRange;
    public float FireTime;
    public float ReloadTime = 1f;

    //other
    private LineRenderer lineRenderer;
    private Vector3 projectileAttachementPoint;
    private Vector3 originPoint;
    private SlingshotState state;

    //calculations
    private Vector2 firingPoint;
    private Vector2 firingDirection;
    private float percPower;
    private ColliderTrigger2D lastShotProjectile;
    private Camera cam;
    
    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
        state = SlingshotState.Loaded;
        cam = Camera.main;
        projectileAttachementPoint = originPoint = new Vector3((RubberAnchorOne.position.x + RubberAnchorTwo.position.x )/2, (RubberAnchorOne.position.y + RubberAnchorTwo.position.y) / 2, 0);
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case SlingshotState.Loaded:
                if(Input.GetMouseButton(0))
                {
                    SetState(SlingshotState.Drag);
                }
                //can drag
                break;
            case SlingshotState.Fired:
                //can't drag
                break;
            case SlingshotState.Drag:
                if(!Input.GetMouseButton(0))
                {
                    //Fire
                    SetState(SlingshotState.Firing);
                }
                //dragging
                OnDrag();
                break;
        }
        projectilePreviewRef.transform.position = projectileAttachementPoint;
        lineRenderer.SetPositions(new Vector3[] { RubberAnchorOne.position, projectileAttachementPoint, RubberAnchorTwo.position});
    }

    private float elapsedTime = 0;
    private void FixedUpdate()
    {
        if(state == SlingshotState.Reloading)
        {
            if(elapsedTime < ReloadTime)
            {
                elapsedTime+= Time.deltaTime;
            }
            else
            {
                elapsedTime = 0;
                projectilePreviewRef.SetActive(true);
                SetState(SlingshotState.Loaded);
            }
        }
        if (state == SlingshotState.Firing)
        {
            if (Vector2.Distance(projectileAttachementPoint, originPoint) < 0.1f)
            {
                //Ball is close enough
                GameObject projectile = Instantiate(ProjectilePrefab, projectileAttachementPoint, Quaternion.identity);
                Rigidbody2D rb = projectile.GetComponent<Rigidbody2D>();
                lastShotProjectile = projectile.GetComponent<ColliderTrigger2D>();
                lastShotProjectile.OnCollision.AddListener(ReloadSlingshot);
                rb.AddForce(firingDirection * percPower * PowerScaling);
                projectilePreviewRef.SetActive(false);
                state = SlingshotState.Fired;
                elapsedTime = 0;
                ProjectileCam.Follow = projectile.transform;
                SwitchCamera("ProjectileCamera");
            }
            else
            {
                //ball is still traveling with rubber
                elapsedTime += Time.deltaTime;
                projectileAttachementPoint = Vector2.Lerp(firingPoint, originPoint, elapsedTime / (FireTime * (1.1f-percPower)));
            }
        }


    }

    private void OnDrag()
    {
        Vector2 screenPos = cam.ScreenToWorldPoint(Input.mousePosition);
        Vector2 direction = (Vector2)originPoint - screenPos;
        float distance = direction.magnitude;
        firingDirection = direction.normalized;
        

        Vector2 clampedPosition;
        if (distance > DragRange)
        {
            //Clamp movement to a circe
            clampedPosition = (Vector2)originPoint + (-1f * direction * DragRange / distance);
        }
        else
        {
            clampedPosition = screenPos;
        }

        percPower = ((Vector2)originPoint - clampedPosition).magnitude / DragRange;
        projectileAttachementPoint = firingPoint = clampedPosition;
    }

    private void ReloadSlingshot()
    {
        lastShotProjectile.OnCollision.RemoveListener(ReloadSlingshot);
        lastShotProjectile = null;
        SetState(SlingshotState.Reloading);
        SwitchCamera("SlingshotCamera");
        
    }

    private void SetState(SlingshotState state)
    {
        this.state = state; 
    }

    private void SwitchCamera(string cameraName)
    {
        CameraAnimController.Play(cameraName);
    }
}